trap dump_date_to_file \
    SIGINT \
    SIGTERM \
    SIGKILL \
    SIGSTOP \
    EXIT

dump_date_to_file() {
    if [[ -z "${WORK_DIR}" || ! -r "${WORK_DIR}" ]]; then
        echo "WORK_DIR variable not defined." >&2
        return 1
    fi
    date '+%s' > "${WORK_DIR}/status"
}
