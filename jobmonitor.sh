#!/bin/bash
#: jobmonitor.sh will run every two or three minutes.
#: this script will monitor execution of several scripts and
#: report if they haven't ran successfully for a specific time.

readonly WORK_DIR=$(dirname $(readlink --canonicalize-existing "${0}" 2> /dev/null))
readonly JOBS_FILE="${WORK_DIR}/jobs.txt"
readonly CONF_FILE="${WORK_DIR}/jobmonitor.conf"
readonly ERROR_BASHLIB_MISSING=78
readonly ERROR_CONF_FILE_NOT_FOUND=79

monitoring_enabled() {
    [[ ! -f "${1}/nomonitor" ]]
}

parse_job_schedule_from_text() {
    local -r JOB_LINE="${1}"
    [[ -z "${JOB_LINE}" ]] && return 1

    jb_path=$(awk -F ',' '{print $1}' <<< "${JOB_LINE}")
    jb_seconds_threshold=$(awk -F ',' '{print $2}' <<< "${JOB_LINE}")
}

threshold_passed() {
    (((${1} - ${2}) > ${3}))
}

. "/az/srvr/jobs/bashecomlib/bashecomlib.sh" > /dev/null || {
    printf "ERROR - '%s' needs the bashecomlib.\n" "${0}" >&2
    exit ${ERROR_BASHLIB_MISSING}
}

. "${CONF_FILE}" || {
    printf "Configuration file '%s' not found.\n" "${JOBS_FILE}" >&2
    exit ${ERROR_CONF_FILE_NOT_FOUND}
}

while read job; do
    
    parse_job_schedule_from_text "${job}" || {
        printf "Error trying to parse information for [%s]\n" "${job}"
        continue
    }

    if monitoring_enabled "${jb_path}"; then
        printf "Monitoring is disabled for '%s'\n" "${jb_path}"
        continue
    fi

    if [[ -f "${jb_path}/status" ]]; then
        if threshold_passed $(date '+%s') $(cat "${jb_path}/status") "${jb_seconds_threshold}"; then
            echo "Threshold has passed ... " #email notification
        fi
    else
        printf "Status file not found [%s], skipping job ...\n" "${jb_path}/status"
    fi

done < <(grep --invert-match --extended-regexp '^#' "${JOBS_FILE}")

exit 0
